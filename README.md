# Gradle JavaFX on Docker #

Docker image containing OpenJDK 8 with OpenJFX package installed for JavaFX support and Gradle.

This was developed to be used by the CI to buid and test our [SIM-ScALA-BIM project](https://gitlab.com/abra-team/sim-scala-bim/).

To use it in GitLab CI, simply add it to [`image: `](https://docs.gitlab.com/ee/ci/yaml/#image-and-services) key in `.gitlab-ci.yml` configuration file:

```YAML
image: niccomlt/gradle-javafx
```
