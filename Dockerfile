FROM gradle:4.10.2-jdk8

USER root

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install openjfx

CMD ["/bin/bash"]